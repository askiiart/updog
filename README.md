# Updog

## What's updog?

Not much, you?

<small>(it's a simple, extensible uptime monitor)</small>

## Warning

**Extensions will be executed with no safety checks!** Make sure an extension isn't malicious before adding it.

## Docs

Docs are in the `./docs/` folder, but here's a quick overview on how to use this:

1. Clone this repository - `git clone --depth 1 https://git.askiiart.net/askiiart/updog`
2. Install your extensions in the appropriate folders: `./extensions/checkers`, `./extensions/alerts` (optional), and `./extensions/logging`
3. Create your `services.json` file, example below.

```json
{
    "global-args": {
        "default-checker": "CheckerTemplate",
        "default-alerts": "AlertsTemplate",
        "default-logging": "LoggingTemplate"
    },
    "site": {
        "name": "A Website",
        "checker": "HttpChecker",
        "checker-args": {
            "url": "https://example.com"
        },
        "rate": 1,
        "alerts-args": {
            "url": "https://example.com/webhook-url-or-whatever-goes-here",
            "lol": "irrelevant, AlertsTemplate ignores these options lol"
        },
        "logging-args": {
            "file": "/home/askiiart/Documents/updog/logs/site-log"
        }
    }
}
```

## To-do

- Add maintenance windows (optionally recurring)
- Add groups
- Add ability for checkers to have preferred alerts and logging extensions - will print a warning if the preferred extension(s) are not used
- Add "keywords" to be replaced in the config (for ~~current directory~~, service name, maybe some others)
  - Current directory done - `%dir%`

---

***All specs are still a work-in-progress, breaking changes are likely!***

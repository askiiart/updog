# Docs

Welcome to the docs. Not docks, [no docker yet](#docker).

## Usage

### Extensions

First, find some extensions. You'll need a checker to check whether something is up, a logging extension to log stuff, and optionally an extension to send alerts. Extensions are specified on a per-service basis, so you can mix-and-match extensions as much as you want. Once there are some real extensions I'll list some here.

### Config

Then create a `services.json` file for your config, like this:

```json
{
    "global-args": {
        "default-checker": "CheckerTemplate",
        "default-alerts": "AlertsTemplate",
        "default-logging": "LoggingTemplate"
    },
    "site": {
        "name": "A Website",
        "checker": "HttpChecker",
        "checker-args": {
            "url": "https://example.com"
        },
        "rate": 1,
        "alerts-args": {
            "url": "https://example.com/webhook-url-or-whatever-goes-here",
            "lol": "irrelevant, AlertsTemplate ignores these options lol"
        },
        "logging-args": {
            "file": "/home/askiiart/Documents/updog/logs/site-log"
        }
    }
}
```

#### Global config

Optional arguments:

- `default-checker`: The default checker for all services, can be overriden per-service using `checker`
- `default-alerts`: The default alerter extension for all services, can be overriden per-service using `alerts`
- `default-logging`: The default logging extension for all services, can be overriden per-service using `logging`

#### Service config

Required arguments:

- The key (in this case `site`): A unique string to identify the service in logs, also used internally.
- `checker`: The name of the checker extension's *class* - optional if `default-checker` is specified in the global arguments.
- `checker-args`: Arguments for the checker. Check the checker's docs for what to put here.
- `rate`: How often to check if the service is up (every *x* seconds)
- `alerts`: The name of the alerting extension's *class* - optional if `default-alerts` is specified in the global arguments.
- `alerts-args`: Arguments for the alerting extension. Check the alerter's docs for what to put here.
- `logging`: The name of the logging extension's *class* - optional if `default-logging` is specified in the global arguments.
- `logging-args`: Arguments for the logging extension. Check the logger's docs for what to put here.

Optional arguments:

- `name`: A human-friendly name for the service; currently unused.

#### Keywords

Keywords get replaced with their value.

- `%dir%`: The directory Updog is located in.

---

This example uses the template extensions, [askiiart/updog-checker_template](https://git.askiiart.net/askiiart/updog-checker_template), [askiiart/updog-logging_template](https://git.askiiart.net/askiiart/updog-logging_template), and [askiiart/updog-alerts_template](https://git.askiiart.net/askiiart/updog-alerts_template), so the arguments for them are mostly irrelevant. For each extension, check its docs for what arguments to use.

### Running it

Just run it with `python3 updog.py`

### Docker

No Docker yet, Updog throws `RuntimeError: can't create new thread at interpreter shutdown` when I try to run it in Docker. So no Docker until I figure that out.

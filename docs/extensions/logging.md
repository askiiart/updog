# Logging Extensions

Updog doesn't do any monitoring by itself. Instead, extensions are used to check the status of whatever thing, send alerts, and log stuff. Updog just chains them together.

Example extension: [askiiart/updog-logging_template](https://git.askiiart.net/askiiart/updog-logging_template)

## Folder layout

Extensions need to be put in the `./extensions/logging` folder, and the name of the file must match the name of the folder.

```txt
.
├── extensions
│   ├── logging
│       └── logging_template
│           └── logging_template.py
```

## Methods

`*`: indicates a method is required

### `__init__()`*

**Arguments**:

- A dict of arguments from `logging-args` in `services.json` - For no arguments, an empty dict will be used

**Return**: None

### `log()`*

**Arguments**: `service_name`, `code`, `status`

- `service_name`: The name of the service the logger is logging - currently it's always the same value for a single instance, but this is required for compatibility when support for logging multiple things with one instance of the logger is supported.
- `code`: The status code returned by the checker
- `status`: The status message for that status code

**Return**: Nothing

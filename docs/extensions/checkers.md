# Uptime Checker Extensions

Updog doesn't do any monitoring by itself. Instead, extensions are used to check the status of whatever thing, send alerts, and log stuff. Updog just chains them together.

Example extension: [askiiart/updog-checker_template](https://git.askiiart.net/askiiart/updog-checker_template)

## Folder layout

Extensions need to be put in the `./extensions/checkers` folder, and the name of the file must match the name of the folder.

```txt
.
├── extensions
│   ├── checkers
│       └── checker_template
│           └── checker_template.py
```

## Methods

`*`: indicates a method is required

### `__init__()`*

**Arguments**:

- (dict) Arguments from `checker-args` in `services.json` - For no arguments, an empty dict will be used

Example:

```py
{
    "url": "https://example.net",
    "port": 443
}
```

**Return**: None

### `get_status()`*

**Arguments**: None

**Return**: An integer indicating status; the codes; what the code means is provided by [`get_return_codes()`](#get_return_codes)

These values can be overriden by providing [`get_return_codes()`](#get_return_codes)

### `get_return_codes()`*

**Arguments**: None

**Return**: A `dict` containing integers and their associated statuses.

Example:

```py
{
    0: "Down",
    50: "Partial outage",
    100: "Up"
}
```

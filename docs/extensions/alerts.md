# Alerts Extensions

Updog doesn't do any monitoring by itself. Instead, extensions are used to check the status of whatever thing, send alerts, and log stuff. Updog just chains them together.

Example extension: [askiiart/updog-alerts_template](https://git.askiiart.net/askiiart/updog-alerts_template)

## Folder layout

Extensions need to be put in the `./extensions/alerts` folder, and the name of the file must match the name of the folder.

```txt
.
├── extensions
│   ├── alerts
│       └── alerts_template
│           └── alerts_template.py
```

## Methods

`*`: indicates a method is required

### `__init__()`*

**Arguments**:

- (dict) Arguments from `alerts-args` in `services.json` - For no arguments, an empty dict will be used

**Return**: None

### `alert()`*

**Arguments**:

- (str) The name of the service the alert is for
- (int) The status code returned by the checker
- (str) The status message for that status code

**Return**: Nothing

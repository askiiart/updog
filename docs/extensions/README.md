# Extensions

Extensions go in the `./extensions/` folder, where exactly depends on which type of extension.

- Extensions for checking status ("checkers") go in `./extensions/checkers/`
- Extensions for alerting go in `./extensions/alerts/`
- Extensions for logging go in `./extensions/logging/`

Note that the folder name *must* be the same as the file name for the extension (excluding .py)!

## Example

```txt
.
├── extensions
│   ├── alerts
│   │   └── alerts_template
│   │       └── alerts_template.py
│   ├── checkers
│   │   └── checker_template
│   │       └── checker_template.py
│   └── logging
│       └── logging_template
│           └── logging_template.py
```

## List of extensions

There are none yet, so consider this section a placeholder

### Checkers

- [askiiart/updog-http_checker](https://git.askiiart.net/askiiart/updog-http_checker) - a basic checker for HTTP stuff

### Alerts

### Logging

## Extension Development

Extensions are simple to develop, check the other markdown files in this directory for details.
